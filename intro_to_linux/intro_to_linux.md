### Introduction to Linux

**Recap**
•Shell, pipes and redirection
•Skeleton structures of python filter program which reads from stdin and writes to stdout

### Lecture 3

Shell cont. what happens when logging in...

Command line to access password files, 
more /etc/passwd or /etc/shadow

•Trace execution of the command line
•Environment variables
•/etc/profile, ~/bash_profile, ~/bashrc
•man bash

export EDITOR=emacs

# Shortcuts in shell

• Wildcards and pattern matching
	      *ranges[]
	         *^(caret)-beggining of line
		    *$(dollar sign) - end of line
		       *echo to test
		          *nb - shell expands, not invoked programs
			     *man 7 glob
			        *? - any single character

e.g ls -d *.c --> lists all .c files in the directory without expanding

Last shortcut is to use backticks, ``, e.g ls -l `which python`

## Some deffinitions talking about data

#Bit (from binary data)

•Basic unit in digital computing and communications
•Can be one of two states, 0 or 1
•Many different type of physical representations possible for storage and communication (on-ff switch, flip-flop circuit, distinct levels of voltage or light intensity etc)

#Byte
 
•8-binary digit quantity 
•Value often referred to as two hexadecimal digits - each hex digit can represent 4 bits(2^4 = 16)
•Binary literals prefixed by 0b so 0b00000000 == 0 or 
0,0b11111111 == 255

# Computer representation of text strings - single byte encoding

•Historically, single-byte encoded character sets (alphanumeric + symbols)
•Recall that a byte can hold 256 distinct values (0-255)
•Each distinct value corresponds to a character

#Encoding Unicode code points as a sequence of bytes

•Note that code points take potentially 20 bits to encode thats more than 2 bytes
•3 common schemes; UTF-8, UTF-16 and UTF-32

------------------------------------------------------------------------------------------------------------

### Lecture 4

**Last time**
• Shell: with login sequnce and profile files
• Wildcards and backticks on the command line

**Today**
• Off piste, reflections of strings - information density/ representation tradeoffs
• Numerical encodings for integers and floating point
• Model computers
• File I/O

Fundimentally a string is held in an array and the beggining of the string will have a fixed memory address with subsequent characters being offset from that address by increasing amounts

## Numbers

# Integers

• Typically 32 or 64 bit with signed and unsigned representations
• Python uses bignums i.e 2**64 -1 is biggest number availble to be stored in a 64 bit sequence

# Floating point numbers

m x b^e i.e m times b to the power of e, if we have 64 bits how to we represent one of these

    ___________________________________
    | 1 |       52       |     11     |
    -----------------------------------

1 bit for the plus/minus, 52 for the mantissa and 11 for the base and exponent so what is the largest number we can prepresent given that schematic
Largest number is 2*(2**(2**10))


