# Python Cheat Sheet

Hopefully a complilation of useful python definitions, examples and shortcuts!


## Sequential Date Types

### Lists

•Can be assigned to a variable and are mutable and so can contain mutliple element types

Mutable
>If the type of element contained within a particular data type can be changed, i.e. from an integer to a string
>this data type is said to be mutable, and inmutable for vice versa

```python
#for a complex list
complex_list = [["a", ["b",["C", "x"]]]]
complex_list[0][1]
["b",["C", "x"]]
complex_list[0][1][1][0]
"C"
```

### Tuples

•Immutable list represented by () parentheses rather than []

•Faster than lists

•Protects data against accidental changes

•Tuples can be used as keys in dictionaries


### Slicing

•Everytime you want to extract part of a string or list
•Represented by [:] so for example [0:6] is first 6 characters of tuple, or [5:] is start counting at 5
•In addition works for three arguements as well for example s[begin: end: step:]

**Length or len() can be used to give the number of characters in a sequence**

## List manipulations

A list can be seen as a stack in computer science, this is a data structure which has at least two operations: One which can be used to put or push data on the stack and another to take away the most upper element of the stack

### Pop and append

```python
s.append()
```

Will simply add whatever is in the parantheses on the end of the sequence s, often it is enough to simply type s afterwards as s.append() returns as None if in print()

```python
s.pop(i)
```

Returns the ith element of a list and then removes it from that list as well and produces a new list

### Extend

•A literal expansion on the append() function, essentially .extend() will add either one more character or anotehr phrase or list on the specified list

```python
colour_size = []
colours = ["red", "white", "blue"]
sizes = [8,9, 10]
colours.extend(sizes)
colours_size = colours

Output--> ["red", "white", "blue", 8, 9, 10]
```

### Index

**Find the position of an element in a list**

•s.index(x[, i[, j]])

•x is the object to be found, i is the starting postion and if given j is the end point specified


### Insert

•Adds elements in arbitrary positions inside the list

•s.insert(index, object)


## Dictionaries


•Unordered sets, accessed via keys not their position

•The example below shows how to set up and access a dictionary

```python
#setting up the dictionary

en-ger = {"red" : "rot", "green" : "grün", "blue" : "blau", "yellow":"gelb"}

#or using tuples as keys

dic = { (1,2,3):"abc", 3.1415:"abc"}
dic
Output--> {3.1415: 'abc', (1, 2, 3): 'abc'}
```
The "in" operator can be used to test if a certain key is located imside a dictionary with the code returning True or False

### Iterating over a dictionary
```python
#Could use

for key in d:
    print(key)

#or use method keys()

for key in d.keys():
    print(key)

#or for the values rather than the keys

for value in d.values()
    print(value)
```

## Lambda, map, filter, reduce

List comprehension is probably a more ideal alternative however the general syntax is quite useful and important to know and recognise

•The syntax is given by -- lambda arguement_list: expression

•For example;

```python
sum = lambda x, y: x + y
sum(3, 4)

Output --> 7
```

### The map() function

•General form of r = map(func, seq)

•The previously defined function def() is put where func is and the seq is usually a predefined list

•If used in conjunction with lambda the syntax id r = list(map(lambda x: (equation), seq) will return a list of the elements in the seq with  the equation performed on each individually

```python
#Given three lists apply an operation to them

a = [1, 2, 4, 6]
b = [5, 7, 8, 9]
c = [-2, 3, -1, 10]

sum = list(map(lambda x, y, z: x+y+z, a, b, c)
print(sum)
```

If mapping a list of functions the same idea can be applied using instead:

```python

from math import sin, cos, tan, pi

def map_fucntions(x, functions):
    return [ func(x) for func in functions ]

family_of_functions = (sin, cos, tan)
print(map_functions(pi, family_of_functions)
```

### Filtering

•Written as filter(fucntion, sequence)

•Literally filters out all the elements of a sequence for which the function returns true

•For example for a sequence of numbers in order to print only odd elements

```python
num = [1, 2, 3, 4, 5, 6]
odd_num = list(filter(lambda x: x%2, num))
print(odd_num)
```

### Reduce

•I think try not to use as there often seems a simpler solution to each scenerio but in essence it applies a function to the first two elements in a list and then will apply the function to the value just generated

```python
from functools import reduce
num = [34, 2, 49, 204, 34]
f= lambda a,b: a if (a>b) else b
reduce(f, num)
```


