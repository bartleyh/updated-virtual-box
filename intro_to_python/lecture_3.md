# Python - lecture 3

More on functions -> recursive functions (they call themselves)
Higher Order functions -> take functions as arguments

### Revisiting Herons method

•Want to use internal functions that read better and simplify the maths by splitting it up

•Recursion, used the factorial example

```python
def fact(n)
    if n <= 1
        return 1
    else:
        return n*fact(n-1)

or for a palindrome
```

```python
def is_palindrome(string):
    if len(string) <= 1:
        return True
    if string[0] == string[-1]:
        return is_palindrome(string[1:-1])
    else:
        return False

Output --> hrhr
```

In order to test this code use

```python
test_cases = ("hello", "rats live on no evil star")

for s in test_cases:
    print(repr(is_palindrome(s)), s)
```

## Higher order functions

using the cube function

```python
def summation(start, end, func):
    result = ""
    for i in range(start, end+1):
        result += func(i)
    return result
```

### Modules

•A module in python is a file containing defintions and statements
Modules are imported

### Tuples

•Any element type can be used and they are imutable i.e unchangeable once defined, found in ()

### Ranges

•It looks like a range returns a tuple but its an illusion, its actually a function
