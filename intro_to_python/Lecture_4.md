# Python - Lecture 4

## Quick look at list comprehensions

Simple case scenario would be:

```python
evens = (0, 2, 4, 6, 8, 10)
odds = [x+1 for x in evens]
odds

Output -->[1, 3, 5, 7, 9, 11]
```

Or in the case of cartesian products

```python

colours =('black', 'red', 'white')
sizes =('S', 'M', 'L')
[(colour, size) for colour in colours for size in sizes]
```

A key/value pair is two objects with one having more importance than the other

### Dictionary

```python
cars = {}

cars['phil'] = 'BMW' 
cars['anna'] = 'fiat'
cars['james'] = BMW

print(cars)

#testing for entry:
'anna' in cars
--> True
```


```python
wordcounts = {}

if word in wordcounts:
    wordcounts[word] +=1
else:
    wordcounts[word] = 1
```



# Python lecture 5

Expression generators only iterate a value without storing so use less memory but if a list is to be used throughout the rest of the program list comprehension is better (i.e. stores the result of a calculation

## Programming errors

• Syntax errors - grammatically incorrect code, caught at "compile time" i.e. before program starts running
• Run-time errors - pyhton encounters erros it can't handle, in this case an exception is raised

## Exceptions

• Don't have to be fatal can be caught and handled

```python
try:
    you do your operations here
except Exceptiona1:
    if there is Exceptiona1, then execute this block.
except Exception2:
    if there is Exception2, then execute this block.
```

```python
import sys
carcount = {}
for line in sys.stdin
    car = line.rstdip
    try:
        carcount[car] += 1
    except Keycount error
        carcount[car] = 1
print(repr(carcount))

## Throwing exceptions with raise()

• Construct new exception object with message
• "raise" the exception
• Can pass failure data conveniently back to "shallower" caller across nested fucntion calls
• Can re-raise exception:

```python
except Exception as e:
    do something
    raise(e)
```

```python
try:
     a = 1/0
except Exception as e:
    (exception, instance)
```

## Assert

• Assert boolean (throws assertionerror)
• assert Boolean, message


# Lecture 6

## Objects

Any one of the fundimental types for example int, str, tuple, essentially the "core things that Python programs manipulate

Every object has a type taht defines what can be done with that object, the objects behaviours (what functions we can call on it) these functions can be basic operations such as plus or minus as well as functions such as len


## Abstract data types

• An ADT is a set of object and operations on those objects
• These operations define an interface, an abstraction

### Example: Are rational numbers ADTs?

• Yes, we discovered we could hide the rational number as long as we had a method of extracting it and implementing in various ways

## Classes

• Classes are Pythons mechansim for making new types
• Class definition creates an object of type type:

• Methods - function definitions inside classes
• Instantiation (construction) - creat a new object of the class type
• Attribute reference - dot notation to access methods

### Special class names

#Lecture 7

##Class (vs Instance) Variables

```python
class Dog:

    kind = "canine"		# class variable shared by all instances

    def __init__(self, name):
        self.name = name	# instance variable unique to each instance

    def __str__(self,):
        return "{0}{1}".format(self.kind, self.name)
```

##Instances

```python
class Foo():
    def __init__(self):
        print("doing nothing")
```
