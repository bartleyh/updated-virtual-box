import random

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    print('\n')
    return wordlist


def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# -----------------------------------


def dashed_lines():
    dashes = ("_") * len(secret_word)
    dashed_line = []
    for dash in dashes:
        dashed_line.append(dash)
    return dashed_line


def game_play():

    print(dashed_lines())
    print('\n')
    guesses_left = 24
    current_guesses = []
    dashed_line = dashed_lines()

    while guesses_left > 0 and not dashed_line == list(secret_word):
        print("You have", guesses_left, "guesses left")
        print('\n')

        letter_guessed = (input("Enter a guess : "))
        print('\n')

        if len(str(letter_guessed)) != 1:
            print("Please enter a single letter only")
            print('\n')
            continue

        elif letter_guessed not in available_letters:
            print("Please input from the available remaining letters")

        elif type(letter_guessed) != str:
            print("Please enter a valid single character")
            print('\n')
            continue

        elif letter_guessed in secret_word:
            print("You guessed a letter!")
            print('\n')

        else:
            print("The letter you guessed wasn't in the secret word!")
            print('\n')

        amend_dashes(secret_word, dashed_line, letter_guessed)
        print('\n')
        guesses_left -= 1
        current_guesses.append(letter_guessed)
        print("Letters already guessed : ", current_guesses)

    if guesses_left == 0:
        print("You lose! The secret word was", secret_word)
    else:
        print("Well done you win! The secret word was", secret_word)


def amend_dashes(secret, dashed_line, guess):
    if guess in secret:
        for n in range(len(secret)):
            if secret[n] == guess:
                dashed_line[n] = guess

    print(dashed_line)


available_letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

wordlist = load_words()
secret_word = choose_word(wordlist)
game_play()